# README #

SimpleSyndicate.Xml NuGet package.

Get the package from https://www.nuget.org/packages/SimpleSyndicate.Xml

### What is this repository for? ###

* Common functionality for working with XML.

### How do I get started? ###

* See the documentation at http://gazooka_g72.bitbucket.org/SimpleSyndicate

### Reporting issues ###

* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate.xml/issues?status=new&status=open
