﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Xml;

namespace SimpleSyndicate.Xml
{
    /// <summary>
    /// The <see cref="XmlElementExtensions"/> class contains methods that extend the <see cref="XmlElement"/> class.
    /// </summary>
    public static class XmlElementExtensions
    {
        /// <summary>
        /// Creates an attribute with the specified <paramref name="name"/> and <paramref name="value"/> and appends it to the node.
        /// </summary>
        /// <param name="element">The <see cref="XmlNode"/> instance that this method extends.</param>
        /// <param name="name">Attribute name.</param>
        /// <param name="value">Attribute value.</param>
        /// <returns>The added <see cref="XmlAttribute"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="element"/> is <c>null</c>.</exception>
        public static XmlAttribute AppendAttribute(this XmlElement element, string name, string value)
        {
            if (element == null)
            {
                throw new ArgumentNullException(nameof(element));
            }

            var attribute = element.OwnerDocument.CreateAttributeWithValue(name, value);
            element.Attributes.Append(attribute);
            return attribute;
        }
    }
}
