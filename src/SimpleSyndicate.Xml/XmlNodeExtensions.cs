﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Xml;

namespace SimpleSyndicate.Xml
{
    /// <summary>
    /// The <see cref="XmlNodeExtensions"/> class contains methods that extend the <see cref="XmlNode"/> class.
    /// </summary>
    public static class XmlNodeExtensions
    {
        /// <summary>
        /// Creates an attribute with the specified <paramref name="name"/> and <paramref name="value"/> and appends it to the node.
        /// </summary>
        /// <param name="node">The <see cref="XmlNode"/> instance that this method extends.</param>
        /// <param name="name">Attribute name.</param>
        /// <param name="value">Attribute value.</param>
        /// <returns>The added <see cref="XmlAttribute"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="node"/> is <c>null</c>.</exception>
        public static XmlAttribute AppendAttribute(this XmlNode node, string name, string value)
        {
            if (node == null)
            {
                throw new ArgumentNullException(nameof(node));
            }

            var attribute = node.OwnerDocument.CreateAttributeWithValue(name, value);
            node.Attributes.Append(attribute);
            return attribute;
        }

        /// <summary>
        /// Replaces the child with the specified <paramref name="name"/> with the <paramref name="newNode"/>; if the child does not exist, the
        /// <paramref name="newNode"/> is simply appended.
        /// </summary>
        /// <param name="node">The <see cref="XmlNode"/> instance that this method extends.</param>
        /// <param name="name">Name of child node.</param>
        /// <param name="newNode">Node to replace existing child with.</param>
        /// <returns>The added <see cref="XmlNode"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="node"/> is <c>null</c>.</exception>
        public static XmlNode ReplaceChild(this XmlNode node, string name, XmlNode newNode)
        {
            if (node == null)
            {
                throw new ArgumentNullException(nameof(node));
            }

            // remove the existing child if it exists
            var existingNode = node.SelectSingleNode("//" + name);
            if (existingNode != null)
            {
                node.RemoveChild(existingNode);
            }

            // add new one in
            return node.AppendChild(newNode);
        }
    }
}
