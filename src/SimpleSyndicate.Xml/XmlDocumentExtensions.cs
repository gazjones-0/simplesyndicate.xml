﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Xml;

namespace SimpleSyndicate.Xml
{
    /// <summary>
    /// The <see cref="XmlDocumentExtensions"/> class contains methods that extend the <see cref="XmlDocument"/> class.
    /// </summary>
    public static class XmlDocumentExtensions
    {
        /// <summary>
        /// Creates an <see cref="XmlAttribute"/> with the specified <paramref name="name"/> and <paramref name="value"/>.
        /// </summary>
        /// <param name="document">The <see cref="XmlDocument"/> instance that this method extends.</param>
        /// <param name="name">Attribute name.</param>
        /// <param name="value">Attribute value.</param>
        /// <returns>The created <see cref="XmlAttribute"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="document"/> is <c>null</c>.</exception>
        public static XmlAttribute CreateAttributeWithValue(this XmlDocument document, string name, string value)
        {
            if (document == null)
            {
                throw new ArgumentNullException(nameof(document));
            }

            var attribute = document.CreateAttribute(name);
            attribute.Value = value;
            return attribute;
        }
    }
}
